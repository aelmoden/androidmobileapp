package com.potatoes.bob.projectapp;

import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class PostsActivity extends AppCompatActivity {

    public static final int USER_LOGGED = 28;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);

        getSupportActionBar().setElevation(0);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(viewPagerAdapter);


        final TabLayout.Tab nearmeposts = tabLayout.newTab();
        final TabLayout.Tab myposts = tabLayout.newTab();
        final TabLayout.Tab carte = tabLayout.newTab();

        myposts.setText(getString(R.string.myposts));
        nearmeposts.setText(getString(R.string.nearmeposts));
        carte.setText(getString(R.string.map));


        tabLayout.addTab(nearmeposts, 0);
        tabLayout.addTab(myposts, 1);
        tabLayout.addTab(carte, 2);

        setListeners();

    }

    public void setListeners()
    {
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
