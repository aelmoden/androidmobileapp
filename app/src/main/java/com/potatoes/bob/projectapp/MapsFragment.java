package com.potatoes.bob.projectapp;


import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MapsFragment extends Fragment {

    MapView mMapView;
    private GoogleMap googleMap;
    private ArrayList<Marker> markerList;
    private ArrayList<LatLng> latLngList;
    private ArrayList<String> titleList;
    private ArrayList<String> descList;
    private Location currentLocation;

    public MapsFragment()
    {
        markerList = new ArrayList<>();
        latLngList = new ArrayList<>();
        titleList = new ArrayList<>();
        descList = new ArrayList<>();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        markerList = new ArrayList<Marker>();

        View rootView = inflater.inflate(R.layout.fragment_maps, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
                LatLng sydney = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                //googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker Title").snippet("Marker Description"));

                for(int i=0;i<latLngList.size();i++) {
                    Marker m = googleMap.addMarker(new MarkerOptions().position(latLngList.get(i)).title("").snippet(""));
                    markerList.add(m);
                    m.showInfoWindow();

                    //markerList.get(i).remove();
                }

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        getActivity().setTitle(getString(R.string.map));

        return rootView;
    }

    public GoogleMap getGoogleMap()
    {
        return googleMap;
    }

    public void removeAllMarkers()
    {
        //googleMap.clear();
        for(int i=0;i<markerList.size();i++)
            markerList.get(i).remove();
    }


    public void addMarker(Location location, String title, String description)
    {
        LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
        latLngList.add(loc);
        titleList.add(title);
        descList.add(description);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {


            for(int i=0;googleMap != null && i<latLngList.size();i++) {
                Marker m = googleMap.addMarker(new MarkerOptions().position(latLngList.get(i)).title(titleList.get(i)).snippet(descList.get(i)));
                markerList.add(m);
                m.showInfoWindow();

                //markerList.get(i).remove();
            }
        }
        else {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        GpsManager gps = GpsManager.getInstance();
        gps.setActivity(getActivity());
        gps.initGps();
        currentLocation = gps.getCurrentLocation();


    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}