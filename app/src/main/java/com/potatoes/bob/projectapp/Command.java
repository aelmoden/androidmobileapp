package com.potatoes.bob.projectapp;

import android.location.Location;

/**
 * Created by student on 16/11/2016.
 */

public interface Command {
    void execute(Location location);
}
