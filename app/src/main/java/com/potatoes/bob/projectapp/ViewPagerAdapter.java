package com.potatoes.bob.projectapp;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private Context mContext;
    private NearPostsFragment nearPostsFragment;
    private MyPostsFragment myPostsFragment;
    private Fragment mapFragment;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public ViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
        mapFragment = Fragment.instantiate(mContext, MapsFragment.class.getName());

        nearPostsFragment = new NearPostsFragment();
        nearPostsFragment.setMapFragment(mapFragment);

        myPostsFragment = new MyPostsFragment();
        myPostsFragment.setMapFragment(mapFragment);
    }

    public MyPostsFragment getMyPostsFragment()
    {
        return myPostsFragment;
    }

    public NearPostsFragment getNearPostsFragment()
    {
        return nearPostsFragment;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0) // Posts
            return nearPostsFragment;
        else if(position == 1)
            return myPostsFragment;
        else // Carte
            return mapFragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    public Fragment getMapFragment()
    {
        return mapFragment;
    }

}