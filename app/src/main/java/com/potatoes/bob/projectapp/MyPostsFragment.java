package com.potatoes.bob.projectapp;

import android.Manifest;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import static com.potatoes.bob.projectapp.Constants.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyPostsFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private AQuery aq;
    private ListView listPostNearMe;
    private ArrayList<HashMap<String, String>> list;
    private EditText postMessage;
    private ImageButton newPostButton;
    private MapsFragment mapFragment;
    private Location currentLocation;
    private boolean firstTimeShown = false;
    private GpsManager gpsManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean firstTimeFragmentShown = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_posts, container, false);

        postMessage = (EditText) view.findViewById(R.id.newPostMessage2);
        listPostNearMe = (ListView) view.findViewById(R.id.listPost2);
        newPostButton = (ImageButton) view.findViewById(R.id.newPostButton2);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout2);

        list = new ArrayList<HashMap<String, String>>();

        postMessage.clearFocus();

        setListeners();

        getActivity().setTitle(getString(R.string.myposts));

        gpsManager = GpsManager.getInstance();
        gpsManager.setActivity(getActivity());
        gpsManager.initGps();

        return view;
    }

    public void setMapFragment(Fragment f) {
        mapFragment = (MapsFragment)f;
    }

    public void onResume() {

        //loadPosts();

        super.onResume();
    }

    public void getPositionAndSendMessage() {
        try {
            sendMessage();
            loadPosts();

        } catch (SecurityException e) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

    }



    private void setListeners() {
        newPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPositionAndSendMessage();
            }
        });

        listPostNearMe.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadPosts();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    public void loadPosts()
    {
        final Location location = gpsManager.getCurrentLocation();

        if(location == null)
        {
            Toast.makeText(getContext(), "Impossible de récupérer la position, vérifiez les permissions", Toast.LENGTH_SHORT).show();
            return;
        }

        if(mapFragment != null)
            mapFragment.removeAllMarkers();

        String url = Constants.WebAppServer + "/posts/myposts/" + MainActivity.token;

        getView().findViewById(R.id.listPost2).setVisibility(View.GONE);
        getView().findViewById(R.id.loadingPosts2).setVisibility(View.VISIBLE);

        final MyPostsFragment mpf = this;

        aq = new AQuery(getContext());
        aq.ajax(url, JSONArray.class, new AjaxCallback<JSONArray>() {


            @Override
            public void callback(String url, JSONArray jsonArray, AjaxStatus status) {
                list.clear();


                if(jsonArray == null)
                    return;

                for(int i = 0;i < jsonArray.length();i++)
                {
                    try {
                        JSONObject jsonObj =  jsonArray.getJSONObject(i);
                        HashMap<String, String> temp = new HashMap<String, String>();
                        temp.put(TOPIC_COLUMN, jsonObj.getString("content"));
                        temp.put(POPULARITY_COLUMN, jsonObj.getString("popularity"));
                        temp.put(SHARE_COLUMN, "");
                        temp.put(IS_MINE, jsonObj.getString("isMine"));
                        temp.put(MESSAGE_ID, jsonObj.getString("messageId"));
                        temp.put(LAT, String.valueOf(location.getLatitude()));
                        temp.put(LON, String.valueOf(location.getLongitude()));
                        list.add(temp);

                        if(mapFragment != null)
                            addPostToMap(jsonObj);
                    }
                    catch(JSONException e)
                    {
                        System.out.println(e.getMessage());
                    }
                }

                ListViewAdapter listViewAdapter = new ListViewAdapter(getActivity(), list, getContext());
                listViewAdapter.setMyPostsFragment(mpf);
                listPostNearMe.setAdapter(listViewAdapter);
                getView().findViewById(R.id.listPost2).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.loadingPosts2).setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if(firstTimeFragmentShown == true) {
                loadPosts();
                firstTimeFragmentShown = false;
            }
        }
        else {
        }
    }

    public void sendMessage()
    {

        final Location location = gpsManager.getCurrentLocation();

        if(location == null)
        {
            Toast.makeText(getContext(), "Impossible de récupérer la position, vérifiez les permissions", Toast.LENGTH_SHORT).show();
            return;
        }
        String token = MainActivity.token;
        //String url = "https://bobwebappserver.mybluemix.net/posts/add";
        String url = Constants.WebAppServer + "/posts/add";

        JSONObject params = new JSONObject();
        try {
            params.putOpt("token", token);
            params.putOpt("content", postMessage.getText());
            params.putOpt("lat", String.valueOf(location.getLatitude()));
            params.putOpt("lon", String.valueOf(location.getLongitude()));
            params.putOpt("address", Utils.getAddressFromLocation(getActivity(), location));
            params.putOpt("date", Utils.getCurrentDateFormatted());
            params.putOpt("img", "");
        }
        catch (JSONException e) {
            Toast.makeText(aq.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        aq = new AQuery(getContext());
        aq.post(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                try {
                    if (status.getCode() == 200 && json != null && json.getString("status").equals("success")) {
                        Toast.makeText(aq.getContext(), "Message posté avec succès!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(aq.getContext(), status.getError(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e) {
                    Toast.makeText(aq.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void addPostToMap(JSONObject json)
    {
        Location loc = new Location(LocationManager.GPS_PROVIDER);
        try {
            loc.setLatitude(Double.valueOf(json.getString("lat")));
            loc.setLongitude(Double.valueOf(json.getString("lon")));
            mapFragment.addMarker(loc, "P" + json.getString("messageId"), json.getString("content"));
        }
        catch(JSONException e)
        {
            Toast.makeText(aq.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    public static float distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

}