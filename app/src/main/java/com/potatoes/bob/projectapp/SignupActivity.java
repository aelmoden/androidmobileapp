package com.potatoes.bob.projectapp;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.*;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.potatoes.bob.projectapp.R.id.password;
import static com.potatoes.bob.projectapp.R.id.user;

public class SignupActivity extends AppCompatActivity {

    private AQuery aq;
    private EditText user, password;
    private Button registerBtn;
    private boolean isRegistering;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        registerBtn = (Button) findViewById(R.id.register);
        user = (EditText) findViewById(R.id.new_username);
        password = (EditText) findViewById(R.id.new_password);
    }


    public void register(View view) {
        String url = Constants.AuthServer + "/signup";
        final String userText = user.getText().toString();
        final String passwordText = password.getText().toString();
        final Context app = getBaseContext();

        if(isRegistering == true)
            return;

        registerBtn.setEnabled(false);
        user.setFocusable(false);
        password.setFocusable(false);

        isRegistering = true;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("username", userText);
        params.put("password", passwordText);
        aq = new AQuery(this);
        aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                registerBtn.setEnabled(true);
                user.setFocusableInTouchMode(true);
                password.setFocusableInTouchMode(true);
                isRegistering = false;

                if (status.getCode() == 200 || status.getCode() == 400) {
                    try {
                        if(status.getCode() == 200)
                        {
                            Toast.makeText(aq.getContext(), "Inscription réussie !", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else
                        {
                            JSONObject obj = new JSONObject(status.getError());
                            Toast.makeText(aq.getContext(), "Erreur d'inscription : " + obj.getString("msg"), Toast.LENGTH_LONG).show();
                        }
                    } catch(Exception e) {
                        Toast.makeText(aq.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

    }


}