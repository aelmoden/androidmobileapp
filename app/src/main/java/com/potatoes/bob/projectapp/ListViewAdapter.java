package com.potatoes.bob.projectapp;

import static com.potatoes.bob.projectapp.Constants.*;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ListViewAdapter extends BaseAdapter{


    public ArrayList<HashMap<String, String>> list;
    private Activity activity;
    private TextView txtFirst;
    private TextView txtSecond;
    private Button shareButton;
    private TextView txtFourth;
    private ImageView imageView;
    private AQuery aq;
    private Context mContext;
    private NearPostsFragment nearPostsFragment = null;
    private MyPostsFragment myPostsFragment = null;

    public ListViewAdapter(Activity activity,ArrayList<HashMap<String, String>> list, Context c){
        super();
        this.activity=activity;
        this.list=list;
        this.mContext = c;
    }

    public void setMyPostsFragment(MyPostsFragment mpf)
    {
        this.myPostsFragment = mpf;
    }

    public void setNearPostsFragment(NearPostsFragment npf)
    {
        this.nearPostsFragment = npf;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=activity.getLayoutInflater();

        final HashMap<String, String> map=list.get(position);

        if(convertView == null){

            convertView=inflater.inflate(R.layout.layout_row, null);

            txtFirst=(TextView) convertView.findViewById(R.id.topic);
            txtSecond=(TextView) convertView.findViewById(R.id.popularity);
            shareButton=(Button) convertView.findViewById(R.id.shareButton);

            setShareButtonListener(map);
        }


        txtFirst.setText(map.get(TOPIC_COLUMN));
        txtSecond.setText(map.get(POPULARITY_COLUMN));
        if(map.get(IS_MINE).equals("true")){
            //((ImageView) convertView.findViewById(R.id.imgId)).setImageDrawable(convertView.getResources().getDrawable(R.drawable.mymsg));
            txtFirst.setTextColor(convertView.getResources().getColor(R.color.colorPrimary));
        }
        else {
            txtFirst.setTextColor(Color.BLACK);
            //((ImageView) convertView.findViewById(R.id.imgId)).setImageDrawable(convertView.getResources().getDrawable(R.drawable.othermsg));
        }
        //convertView.setBackgroundResource(R.drawable.mymsg);
        //convertView.setS

        return convertView;
    }

    public void setShareButtonListener(final HashMap<String, String> map)
    {
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareMessage(map);
            }
        });
    }

    public void shareMessage(HashMap<String, String> map)
    {
        String url = Constants.WebAppServer + "/posts/share";
        JSONObject params = new JSONObject();
        try {
            params.putOpt("token", MainActivity.token);
            params.putOpt("lat", map.get(LAT));
            params.putOpt("lon", map.get(LON));
            params.putOpt("messageId", map.get(MESSAGE_ID));
        }
        catch (JSONException e) {
            Toast.makeText(aq.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }

        aq = new AQuery(mContext);
        aq.post(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            public void callback(String url, JSONObject jsonObject, AjaxStatus status) {
                if(jsonObject == null)
                    return;

                try{
                    if(jsonObject.getString("status").equals("success"))
                    {
                        Toast.makeText(aq.getContext(), "Message partagé !", Toast.LENGTH_SHORT).show();
                        if(nearPostsFragment != null)
                            nearPostsFragment.loadPosts();
                        else
                            myPostsFragment.loadPosts();
                    }
                }
                catch(JSONException e)
                {
                    Toast.makeText(aq.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

}