package com.potatoes.bob.projectapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static java.security.AccessController.getContext;


public class LoginActivity extends AppCompatActivity {
    private EditText user, password;
    private Button loginBtn;
    private AQuery aq;
    private boolean isLogging;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        loginBtn = (Button) findViewById(R.id.login);
        user = (EditText) findViewById(R.id.user);
        password = (EditText) findViewById(R.id.password);
    }

    public void openRegister(View view)
    {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }

    public void loginUser(View view)
    {
        String url = Constants.AuthServer + "/login";
        final String userText = user.getText().toString();
        final String passwordText = password.getText().toString();
        final Context app = getBaseContext();

        if(isLogging == true)
            return;

        loginBtn.setEnabled(false);
        loginBtn.setText("Connexion en cours...");
        user.setFocusable(false);
        password.setFocusable(false);

        isLogging = true;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("username", userText);
        params.put("password", passwordText);
        aq = new AQuery(this);
        aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                loginBtn.setEnabled(true);
                loginBtn.setText("Se Connecter");
                user.setFocusableInTouchMode(true);
                password.setFocusableInTouchMode(true);
                isLogging = false;

                if (status.getCode() == 200 || status.getCode() == 400) {
                    try {
                        if(status.getCode() == 200)
                        {
                            MainActivity.token = json.getString("token");
                            Toast.makeText(aq.getContext(), "Bienvenue " + userText, Toast.LENGTH_LONG).show();
                            setResult(RESULT_OK);
                            finish();
                            //finishActivity(MainActivity.USER_LOGGED);
                        }
                        else
                        {
                            JSONObject obj = new JSONObject(status.getError());
                            Toast.makeText(aq.getContext(), "Identifiants Incorrects : " + obj.getString("msg"), Toast.LENGTH_LONG).show();
                        }
                    } catch(Exception e) {
                        Toast.makeText(aq.getContext(), "Impossible de se connecter.", Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    Toast.makeText(aq.getContext(), "Impossible de se connecter.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
