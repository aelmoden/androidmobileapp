package com.potatoes.bob.projectapp;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

/**
 * Created by student on 17/11/2016.
 */

public class GpsManager {

    public static Location currentLocation = null;
    private LocationManager locationManager;
    private Activity activity;
    private static GpsManager instance = null;

    private GpsManager()
    {}

    public void initGps()
    {
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
    }

    public void setActivity(Activity act)
    {
        this.activity = act;
    }

    public static GpsManager getInstance()
    {
        if(instance == null)
            instance = new GpsManager();
        return instance;

    }

    public Location getCurrentLocation() {
        Location loc = null;
        if (currentLocation == null) {

            try {
                loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            } catch (SecurityException e) {
                ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                try {
                    loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                } catch (SecurityException e2) {
                    ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
            }
        }

        return loc;
    }

}
