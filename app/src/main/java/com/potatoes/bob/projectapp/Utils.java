package com.potatoes.bob.projectapp;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by student on 17/11/2016.
 */

public class Utils {


    public static String getCurrentDateFormatted() {
        Long tsLong = System.currentTimeMillis() / 1000;
        return tsLong.toString();
    }

    public static String getAddressFromLocation(Activity act, Location location) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(act, Locale.getDefault());
        String address;

        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            address += " " + addresses.get(0).getPostalCode();
            address += " " + addresses.get(0).getLocality();

        } catch (IOException e) {
            Log.e("APP", e.getMessage());
            address = "Unknown Address";
        }

        return address;
    }
}
