package com.potatoes.bob.projectapp;

/**
 * Created by student on 14/11/2016.
 */

public class Constants {
    public static final String TOPIC_COLUMN = "TOPIC";
    public static final String POPULARITY_COLUMN = "POPULARITY";
    public static final String SHARE_COLUMN = "SHARE";
    public static final String IS_MINE = "IS_MINE";
    public static final String MESSAGE_ID = "MESSAGE_ID";
    public static final String LAT = "LAT";
    public static final String LON = "LON";
    public static final String AuthServer = "https://bobappauth.herokuapp.com";
    //public static final String AuthServer = "http://172.30.1.163:3000";
    //https://bobwebappserver.mybluemix.net
    public static final String WebAppServer = "http://172.30.1.76:8090";
    //public static final String WebAppServer = "https://bobwebappserver.mybluemix.net";

}
