package com.potatoes.bob.projectapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.Window;

public class MainActivity extends AppCompatActivity {

    public static String token = "";
    public static final int USER_LOGGED = 28;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent(this, LoginActivity.class);

        startActivityForResult(intent, USER_LOGGED);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == USER_LOGGED) {
            if (resultCode == RESULT_OK) {
                startActivity(new Intent(this, PostsActivity.class));
            }
        }
    }

}