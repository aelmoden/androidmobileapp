package com.potatoes.bob.projectapp;

import android.Manifest;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import static com.potatoes.bob.projectapp.Constants.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class NearPostsFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private AQuery aq;
    private ListView listPostNearMe;
    private ArrayList<HashMap<String, String>> list;
    private EditText postMessage;
    private ImageButton newPostButton;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Spinner dateSpinner;
    private SeekBar distanceSeekBar;
    private int distanceKm;
    private String dateStart;
    private DatePicker datePicker;
    private Location previousLocation = null;
    private MapsFragment mapFragment;
    private Location currentLocation;
    private boolean firstTimeShown = false;
    private GpsManager gpsManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView distanceTextView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_near_me, container, false);

        dateSpinner = (Spinner) view.findViewById(R.id.dateSpinner);
        postMessage = (EditText) view.findViewById(R.id.newPostMessage);
        listPostNearMe = (ListView) view.findViewById(R.id.listPost);
        newPostButton = (ImageButton) view.findViewById(R.id.newPostButton);
        distanceSeekBar = (SeekBar) view.findViewById(R.id.seekBarDistance);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        distanceTextView = (TextView) view.findViewById(R.id.distanceTextView);



        list = new ArrayList<HashMap<String, String>>();

        distanceSeekBar.setProgress(1000);
        distanceKm = distanceSeekBar.getProgress();
        postMessage.clearFocus();

        Date date = new Date(System.currentTimeMillis() - (4 * 60 * 60 * 1000));
        dateStart = String.valueOf(date.getTime()/1000L);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.date, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dateSpinner.setAdapter(adapter);

        setListeners();

        getActivity().setTitle(getString(R.string.nearmeposts));

        gpsManager = GpsManager.getInstance();
        gpsManager.setActivity(getActivity());
        gpsManager.initGps();

        return view;
    }

    public void setMapFragment(Fragment f) {
        mapFragment = (MapsFragment)f;
    }

    public void onResume() {

        loadPosts();

        super.onResume();
    }

    public void getPositionAndSendMessage() {
        try {
            sendMessage();
            loadPosts();


        } catch (SecurityException e) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

    }


    private void setListeners() {
        newPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPositionAndSendMessage();
            }
        });

        listPostNearMe.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });

        distanceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                distanceKm = progress;
                distanceTextView.setText(progress + " Km");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                loadPosts();
            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadPosts();
                swipeRefreshLayout.setRefreshing(false);
            }
        });


        int timpeStamp;


        dateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                if(firstTimeShown == false) {
                    firstTimeShown = true;
                    return;
                }

                String currentElement = adapterView.getItemAtPosition(pos).toString();
                if (currentElement.equals("4 Heures")) {
                    Date date = new Date(System.currentTimeMillis() - (4 * 60 * 60 * 1000));
                    dateStart = String.valueOf(date.getTime()/1000L);
                } else if (currentElement.equals("1 Jour")) {
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, -1);
                    dateStart = String.valueOf(cal.getTimeInMillis() / 1000L);

                } else if (currentElement.equals("3 Jours")) {
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, -3);
                    dateStart = String.valueOf(cal.getTimeInMillis() / 1000L);
                } else if (currentElement.equals("1 Semaine")) {
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, -1);
                    dateStart = String.valueOf(cal.getTimeInMillis() / 1000L);
                }


                loadPosts();


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void loadPosts()
    {
        final Location location = gpsManager.getCurrentLocation();

        if(location == null)
        {
            Toast.makeText(getContext(), "Impossible de récupérer la position, vérifiez les permissions", Toast.LENGTH_SHORT).show();
            return;
        }

        if(mapFragment != null)
            mapFragment.removeAllMarkers();

        String token = MainActivity.token;
        String url = Constants.WebAppServer + "/posts/getPostByDateAndLocation";
        JSONObject params = new JSONObject();
        try {
            params.putOpt("token", token);
            params.putOpt("datestart", String.valueOf(dateStart));
            params.putOpt("lat", String.valueOf(location.getLatitude()));
            params.putOpt("lon", String.valueOf(location.getLongitude()));
            params.putOpt("distanceKm", String.valueOf(distanceKm));
        }
        catch (JSONException e) {
            Toast.makeText(aq.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }

        getView().findViewById(R.id.listPost).setVisibility(View.GONE);
        getView().findViewById(R.id.loadingPosts).setVisibility(View.VISIBLE);

        final NearPostsFragment npf = this;

        aq = new AQuery(getContext());
        aq.post(url, params, JSONArray.class, new AjaxCallback<JSONArray>() {


            @Override
            public void callback(String url, JSONArray jsonArray, AjaxStatus status) {
                list.clear();

                getView().findViewById(R.id.listPost).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.loadingPosts).setVisibility(View.GONE);
                if(jsonArray == null)
                    return;

                for(int i = 0;i < jsonArray.length();i++)
                {
                    try {
                        JSONObject jsonObj =  jsonArray.getJSONObject(i);
                        HashMap<String, String> temp = new HashMap<String, String>();
                        temp.put(TOPIC_COLUMN, jsonObj.getString("content"));
                        temp.put(POPULARITY_COLUMN, jsonObj.getString("popularity"));
                        temp.put(SHARE_COLUMN, "");
                        temp.put(IS_MINE, jsonObj.getString("isMine"));
                        temp.put(MESSAGE_ID, jsonObj.getString("messageId"));
                        temp.put(LAT, String.valueOf(location.getLatitude()));
                        temp.put(LON, String.valueOf(location.getLongitude()));
                        list.add(temp);

                        if(mapFragment != null)
                            addPostToMap(jsonObj);
                    }
                    catch(JSONException e)
                    {
                        System.out.println(e.getMessage());
                    }
                }

                 ListViewAdapter listViewAdapter = new ListViewAdapter(getActivity(), list, getContext());
                listViewAdapter.setNearPostsFragment(npf);
                listPostNearMe.setAdapter(listViewAdapter);

                getView().findViewById(R.id.listPost).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.loadingPosts).setVisibility(View.GONE);
            }
        });
    }

    public void sendMessage()
    {

        final Location location = gpsManager.getCurrentLocation();

        if(location == null)
        {
            Toast.makeText(getContext(), "Impossible de récupérer la position, vérifiez les permissions", Toast.LENGTH_SHORT).show();
            return;
        }
        String token = MainActivity.token;
        //String url = "https://bobwebappserver.mybluemix.net/posts/add";
        String url = Constants.WebAppServer + "/posts/add";

        JSONObject params = new JSONObject();
        try {
            params.putOpt("token", token);
            params.putOpt("content", postMessage.getText());
            params.putOpt("lat", String.valueOf(location.getLatitude()));
            params.putOpt("lon", String.valueOf(location.getLongitude()));
            params.putOpt("address", Utils.getAddressFromLocation(getActivity(), location));
            params.putOpt("date", Utils.getCurrentDateFormatted());
            params.putOpt("img", "");
        }
        catch (JSONException e) {
            Toast.makeText(aq.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        aq = new AQuery(getContext());
        aq.post(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                try {
                    if (status.getCode() == 200 && json != null && json.getString("status").equals("success")) {
                        Toast.makeText(aq.getContext(), "Message posté avec succès!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(aq.getContext(), status.getError(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e) {
                    Toast.makeText(aq.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void addPostToMap(JSONObject json)
    {
        Location loc = new Location(LocationManager.GPS_PROVIDER);
        try {
            loc.setLatitude(Double.valueOf(json.getString("lat")));
            loc.setLongitude(Double.valueOf(json.getString("lon")));
            mapFragment.addMarker(loc, "Post " + json.getString("messageId"), json.getString("content"));
            Log.i("APP", Double.valueOf(json.getString("lat")) + " " + Double.valueOf(json.getString("lat")));
        }
        catch(JSONException e)
        {
            Toast.makeText(aq.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    public static float distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

}